#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import numpy as np
from os import path, getcwd

SCREEN_SIZE = (600,700)

class Block(pygame.sprite.Sprite):
    def __init__(self, position, white_bg=0, color=None):
        pygame.sprite.Sprite.__init__(self)
        self.size = (50, 10)
        self.image = pygame.Surface(self.size)
        self.position = position
        if color is None:
            self.image.set_alpha(170)
            self.color = self.random_color(white_bg)
            self.special = True
        else:
            self.color = color
            self.special = False

        self.image.fill(list(self.color))
        self.rect = self.image.get_rect(topleft=position)

    def place_on(self, x, y):
        return (x, y, self.size[0], self.size[1])

    def random_color(self, white_bg):
        return np.random.randint(50-white_bg, 256-white_bg, 3).astype(int) #50 - to avoid dark blocks



class Paddle(pygame.sprite.Sprite):
    def __init__(self, pattern=None, white_bg=False):
        pygame.sprite.Sprite.__init__(self)
        self.size = [80,10]
        self.position = [360.0, 650.0]
        self.iswhitebg = white_bg
        self.pattern_name = pattern
        self.set_parameters()

    def set_parameters(self):
        self.image = pygame.Surface(self.size)
        self.set_pattern(self.pattern_name)
        self.rect = self.image.get_rect(topleft=self.position)

    def set_pattern(self, pattern):
        if pattern is None or not path.isfile(getcwd()+"/img/"+pattern+".png"):
            if self.iswhitebg:
                self.image.fill([0, 0, 0])  # black paddle
            else:
                self.image.fill([255, 255, 255])  # white paddle
        else:
            self.pattern = pygame.transform.scale(pygame.image.load("img/" + pattern + ".png").convert_alpha(),self.size)
            self.image.blit(self.pattern, (0, 0))

    def move(self, direc):
        self.position[0] += direc

    def change_size(self, to_add):
        if self.size[0] + to_add > 0 and self.size[0] < SCREEN_SIZE[0]/3:
            self.size[0] += to_add
        self.set_parameters()

    def get_ball_position(self, ball_radius):
        return np.array([self.position[0]+self.size[0]/2 - ball_radius, 650-2*ball_radius])

    def draw(self, surface):
        self.rect = self.image.get_rect(topleft=self.position)
        surface.blit(self.image, self.rect)



class Ball(pygame.sprite.Sprite):
    def __init__(self, pattern=None, position=np.array([400,640]), radius=5, acc=1):
        pygame.sprite.Sprite.__init__(self)
        self.radius = radius
        self.position = position
        self.pattern_name = pattern
        self.set_parameters(radius)
        self.accel = acc
        self.direction = np.array([1,-1])
        self.active=False
        self.wallsound = pygame.mixer.Sound(getcwd()+"/sounds/wall.wav")
        self.rect = self.image.get_rect(topleft=self.position)

    def set_parameters(self, radius):
        self.radius = radius
        self.size = [2 * self.radius, 2 * self.radius]
        self.image = pygame.Surface(self.size).convert_alpha()
        self.set_pattern(self.pattern_name)
        self.rect = self.image.get_rect(topleft=self.position)

    def set_pattern(self, pattern):
        if pattern is None or not path.isfile(getcwd()+"/img/"+pattern+".png"):
            self.image.fill([255, 255, 255])  # white ball
            self.pattern = self.image
        else:
            self.pattern = pygame.transform.scale(pygame.image.load("img/"+pattern+".png").convert_alpha(),
                                                  self.size)

    def set_position(self, position, default=False):
        self.position = position
        if default:
            self.direction = np.array([1, -1])

    def move(self):
        if self.active:
            new_position = self.position + self.accel * self.direction
            if new_position[0]< 0:
                self.wallsound.play()
                new_position[0] = -new_position[0]
                self.direction[0] = -self.direction[0]
            elif new_position[0] > SCREEN_SIZE[0]-2*self.radius:
                self.wallsound.play()
                new_position[0] = 2*SCREEN_SIZE[0]-4*self.radius-new_position[0]
                self.direction[0] = -self.direction[0]
            if new_position[1] < 0:
                self.wallsound.play()
                new_position[1] = -new_position[1]
                self.direction[1] = -self.direction[1]
            elif new_position[1] > SCREEN_SIZE[1]:
                return False
            self.position = new_position
            return True

    def draw(self, surface):
        self.rect = self.image.get_rect(topleft=self.position)
        surface.blit(self.pattern, self.rect)



class Bonus(pygame.sprite.Sprite):
    #TYPES = ['large_ball', 'small_ball', 'double ball', 'large_paddle', 'small_paddle', 'speedup', 'sticky', 'life']
    def __init__(self, position):
        pygame.sprite.Sprite.__init__(self)
        self.type = np.random.randint(0,8)
        self.size = [20, 20]
        self.image = pygame.Surface(self.size).convert_alpha()
        position[0] += 20
        self.position = position
        self.pattern = pygame.transform.scale(pygame.image.load("img/bonus" + str(self.type) + ".png").convert_alpha(),
                                              self.size)
        self.rect = self.image.get_rect(topleft=self.position)

    def fall(self):
        self.position[1]+=0.5
        if self.position[1] > SCREEN_SIZE[1]:
            return False
        self.rect = self.image.get_rect(topleft=self.position)
        return True

    def draw(self, surface):
        surface.blit(self.pattern, self.rect)