# Arkanoid #


## Polska wersja ##

### Opis gry ###
Celem gry jest zbicie jak największej liczby klocków na planszy za pomocą odbijanej piłeczki.
Liczba rund jest nieskończona, za każdym razem plansza ładowana jest losowo. Z klocków odznaczających się innym kolorem niż większość na planszy wypadają bonusy (po zbiciu). Szczegółowy opis dostępnych bonusów poniżej.
Gracz na początku ma do dyspozycji 5 żyć, za każdą utraconą piłeczkę traci jedną. Życia można również zyskać po złapaniu odpowiedniego bonusu. Po utraceniu ostatniego życia gra się kończy.
Najlepszy uzyskany wynik jest zapisywany w pamięci.

### Wymagania ###
Do uruchomienia gry potrzebny jest Python 3.5 oraz biblioteki: Pygame oraz Numpy.

### Rozpoczęcie gry ###
Grę najlepiej rozpocząć uruchomieniem pliku setup.py (_python ./setup.py_) - opcjonalne.  Wyświetlone okienko umożliwia wybranie jednej z trzech paletek, jednej z pięciu piłeczek oraz domyślnej prędkości poruszania się paletki i piłki. Aby zaakceptować zmiany, należy nacisnąć SAVE.
Po ustawieniu parametrów gra uruchamiana poprzez plik arkanoid.py (_python ./arkanoid.py_).

### Sterowanie ###
* SPACJA - wypuszczenie piłki 
* LEWA STRZAŁKA - ruch paletki w lewo
* PRAWA STRZAŁKA - ruch paletki w prawo 
* R - restart rozgrywki (wraz z utratą dotychczasowego wyniku)
* ESC - przerwanie gry (wyświetla planszę GAME OVER z liczbą uzyskanych punktów, aby wyjść zupełnie należy raz jeszcze nacisnąć dowolny klawisz)

Uwaga! Zamknięcie okna skutkuje podobnym działaniem jak przycisk ESC. Wymagane dwukrotne wybranie w celu wyjścia.

Okno setup.py obsługiwane jest myszką.


### Dostępne bonusy ###

* ![Bonus0](http://i.imgur.com/2K0Nzsa.png) - powiększenie piłeczki
* ![Bonus1](http://i.imgur.com/r39GNUA.png) - pomniejszenie piłeczki
* ![Bonus2](http://i.imgur.com/YpXBxE3.png) - powiększenie paletki 
* ![Bonus3](http://i.imgur.com/XMAlDjL.png) - pomniejszenie paletki 
* ![Bonus4](http://i.imgur.com/6ypdAqm.png) - lepka paletka (ponowny wyrzut spacją)
* ![Bonus5](http://i.imgur.com/XMiLPeM.png) - dodatkowa piłka 
* ![Bonus6](http://i.imgur.com/SasFNbJ.png) - przyspieszenie piłki 
* ![Bonus7](http://i.imgur.com/CHsvXNe.png) - dodatkowe życie 


Wszystkie złapane bonusy (poza dodatkowym życiem i dodatkową piłką) działają przez 10 sekund.


### Użyte grafiki i dźwięki ###

Grafiki bonusów (+ pliki arrowup.png, arrowdown.png) oraz wszelkie dźwięki zawarte w projekcie zostały stworzone przez twórcę gry oraz zostały przez niego udostępnione do użytku własnego.

Grafiki piłeczek oraz ikona serca zostały użyte w oparciu o wolną licencję strony www.iconfinder.com.

Grafiki paletek, planszy "game over" zostały użyte w oparciu o wolną licencję public domain.



>Powodzenia,

>Marcin Chołoniewski

***

## English version ##

### Description ###
The aim of this game is to smash as many blocks on a board as possible using a ball and a moving paddle.
Rounds are limitless and generated randomly. Blocks with distinguishable colors among others hide some bonuses (listed with details below) that are revealed after smash.
Player has 5 lives at the beginning of a game and loses one for each lost ball. Lives may be also collected by a particular bonus catch. Game is over after last life loss.
The best result (high-score) is saved in memory.

###Dependencies ###
To run the scripts successfully - Python 3.5 is required, as well as its libraries: Pygame and Numpy.

### How to start? ###
The best first move is to run the setup.py file (command: _python ./setup.py_), but it is not obligatory.
User can choose one of three paddles, one of five balls and default paddle or ball speed in displayed setup window.
To accept changes, press SAVE button.
With new parameters set, the game can be run using arkanoid.py file (command: _python ./arkanoid.py_).

### Control ###
* SPACE - release a ball
* LEFT ARROW - move paddle to the left
* RIGHT ARROW - move paddle to the right
* R - restart game (makes all results lost)
* ESC - end a game (displays a game-over board with score, press any key again to quit)

Warning! Closing a window has the same result as ESC button. Double press required to real quit.

Mouse control in setup.py window.

### Available bonuses ###

* ![Bonus0](http://i.imgur.com/2K0Nzsa.png) - large ball
* ![Bonus1](http://i.imgur.com/r39GNUA.png) - small ball
* ![Bonus2](http://i.imgur.com/YpXBxE3.png) - wider paddle
* ![Bonus3](http://i.imgur.com/XMAlDjL.png) - shorter paddle
* ![Bonus4](http://i.imgur.com/6ypdAqm.png) - sticky paddle (released by space)
* ![Bonus5](http://i.imgur.com/XMiLPeM.png) - double ball
* ![Bonus6](http://i.imgur.com/SasFNbJ.png) - faster ball
* ![Bonus7](http://i.imgur.com/CHsvXNe.png) - extra life


All caught bonuses (except extra life and extra ball) last 10 seconds.

### Used graphics and sounds ###

Bonus graphics (+ arrowup.png, arrowdown.png files) and all sounds included in this project were created by the game developer and they are shared for non-commercial use.

Ball graphics and heart icon were used with free license of site www.iconfinder.com.

Paddle graphics, "game over" board were used with public domain license.



>Enjoy,

>Marcin Chołoniewski