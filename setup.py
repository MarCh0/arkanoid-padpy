#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *

SCREEN_SIZE = (400,300)

class Choice(pygame.sprite.Sprite):
    def __init__(self, type, name, rect):
        pygame.sprite.Sprite.__init__(self)
        self.type = type
        self.name = name
        self.rect = rect


def save_settings():
    with open(".settings", 'w') as f:
        for k in settings:
            f.write(k + ":" + settings[k] + "\n")

pygame.init()

screen = pygame.display.set_mode(SCREEN_SIZE)
info_font = pygame.font.Font(None,24)
speed_font = pygame.font.Font(None, 30)
whback_font = pygame.font.Font(None,14)
settings = dict()
paddle_size=[80,10]
paddle_names=['normal', 'paddle_eyes', 'paddle_fur']
paddle_pic=[]
ball_size=[20,20]
ball_names=['pixel', 'ball', 'pool', 'tennis', 'pokeball']
ball_pic=[]
paddle_text = info_font.render("Choose paddle:", 1, [255, 255, 255])
ball_text = info_font.render("Choose ball:", 1, [255, 255, 255])
baccel_text = info_font.render("Default ball speed:", 1, [255, 255, 255])
paccel_text = info_font.render("Default paddle speed:", 1, [255, 255, 255])
done = False

with open('.settings', 'r') as f:
    for line in f:
        pair = line.split('\n')[0].split(":")
        settings[pair[0]] = pair[1]
    f.close()

for name in paddle_names:
    if name=='normal':
        normal_paddle = pygame.Surface(paddle_size)
        normal_paddle.fill([255, 255, 255])
        paddle_pic.append(normal_paddle)
    else:
        paddle_pic.append(pygame.transform.scale(pygame.image.load("img/" + name + ".png").convert_alpha(), paddle_size))

for name in ball_names:
    if name=='pixel':
        pixel_ball = pygame.Surface(ball_size)
        pixel_ball.fill([255, 255, 255])
        ball_pic.append(pixel_ball)
    else:
        ball_pic.append(pygame.transform.scale(pygame.image.load("img/" + name + ".png").convert_alpha(), ball_size))

arrowup = pygame.transform.scale(pygame.image.load("img/arrowup.png").convert_alpha(), [15,15])
arrowdown = pygame.transform.scale(pygame.image.load("img/arrowdown.png").convert_alpha(), [15,15])

background = pygame.Surface(SCREEN_SIZE)
background.fill([0,0,0])

choices=[]
for i, name in enumerate(paddle_names):
    choices.append(Choice('p', name, Rect(25+i*120, 35, 110, 40)))
for i, name in enumerate(ball_names):
    choices.append(Choice('b', name, Rect(25 + i * 80, 120, 40, 40)))
#arrows:
choices.append(Choice('a1', '0.1', Rect(100,220,15,15)))
choices.append(Choice('a1', '-0.1', Rect(35,220,15,15)))
choices.append(Choice('a2', '0.1', Rect(320, 220,15,15)))
choices.append(Choice('a2', '-0.1', Rect(255, 220,15,15)))
#save:
choices.append(Choice('s', 'save', Rect(170,260,60,30)))

fields = pygame.sprite.Group(choices)

while(not done):
    screen.blit(background, (0, 0))

    keys = pygame.key.get_pressed()
    for event in pygame.event.get():
        if event.type == pygame.QUIT or keys[K_ESCAPE]:
            done = True

        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            clicked = [ch for ch in fields if ch.rect.collidepoint(pos)]
            if len(clicked) > 0:
                clicked = clicked[0]
                if clicked.type=='s':
                    save_settings()
                    done = True
                elif clicked.type=='a1':
                    if 0 < float(settings['baccel']) + float(clicked.name) < 2:
                        settings['baccel'] = str(round(float(settings['baccel']) + float(clicked.name),1))
                elif clicked.type=='a2':
                    if 0 < float(settings['paccel']) + float(clicked.name) < 5:
                        settings['paccel'] = str(round(float(settings['paccel']) + float(clicked.name),1))
                elif clicked.type=='p':
                    settings['paddle'] = clicked.name
                elif clicked.type=='b':
                    settings['ball'] = clicked.name


    pos = pygame.mouse.get_pos()
    hoovered = [ch for ch in fields if ch.rect.collidepoint(pos)]
    if len(hoovered) > 0:
        if hoovered[0].type=='s':
            pygame.draw.rect(screen, [255, 255, 255], hoovered[0].rect, 4)
        else:
            pygame.draw.rect(screen, [255, 255, 255], hoovered[0].rect, 1)
        if hoovered[0].name=='pool':
            screen.blit(whback_font.render("+white backgr", 1, [255, 255, 255]), (175, 165))

    screen.blit(paddle_text, (10, 10))

    screen.blit(ball_text, (10,95))

    screen.blit(baccel_text, (10, 190))

    screen.blit(paccel_text, (210, 190))

    for i, pic in enumerate(paddle_pic):
        screen.blit(pic, (40+i*120, 50))
        if paddle_names[i] == settings['paddle']:
            pygame.draw.rect(screen, [0,255,0], Rect(25+i*120, 35, 110, 40), 2)

    for i, pic in enumerate(ball_pic):
        if i==2:
            pygame.draw.rect(screen, [255, 255, 255], Rect(192, 127, 26, 26))
        screen.blit(pic, (35 + i * 80, 130))
        if ball_names[i] == settings['ball']:
            pygame.draw.rect(screen, [0, 255, 0], Rect(25 + i * 80, 120, 40, 40), 2)

    baccel_nbr = speed_font.render(settings['baccel'], 1, [200, 200, 255])
    screen.blit(baccel_nbr, (60, 220))
    screen.blit(arrowup,(100,220))
    screen.blit(arrowdown,(35,220))

    paccel_nbr = speed_font.render(settings['paccel'], 1, [200, 200, 255])
    screen.blit(paccel_nbr, (280, 220))
    screen.blit(arrowup, (320, 220))
    screen.blit(arrowdown, (255, 220))

    pygame.draw.rect(screen, [255,255,255], Rect(170,260,60,30), 2)
    save_text = info_font.render("SAVE", 1, [255, 255, 255])
    screen.blit(save_text, (179, 269))



    pygame.display.flip()
