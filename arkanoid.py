#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
from sys import exit
from os import path, listdir, getcwd
import numpy as np
from items import Block, Paddle, Ball, Bonus


SCREEN_SIZE = (600,700)      # ustalamy rozmiar ekranu

pygame.mixer.pre_init(44100, 16, 2, 4096)
pygame.init()
screen = pygame.display.set_mode(SCREEN_SIZE)
info_font = pygame.font.Font(None,24)
end_font = pygame.font.Font(None,80)
endscore_font = pygame.font.Font(None,48)
mini_font = pygame.font.Font(None, 14)
round_clock = pygame.time.Clock()
pygame.time.set_timer(USEREVENT + 1, 1000)
sounds = dict()
settings = dict()

for file in listdir(getcwd()+"/sounds"):
    if path.isfile(getcwd()+"/sounds/"+file):
        sounds[file.split('.')[0]] = pygame.mixer.Sound(getcwd()+"/sounds/"+file)

with open('.settings', 'r') as f:
    for line in f:
        pair = line.split('\n')[0].split(":")
        settings[pair[0]] = pair[1]
    f.close()


round = 1
lives = 5
score = 0
seconds = 0
sticky = False
done = False
active_bonuses = {0:-1, 1:-1, 2:-1, 3:-1, 4:-1, 5:-1, 6:-1}
heart = pygame.transform.scale(pygame.image.load("img/heart.png").convert_alpha(), [15,15])
koala = pygame.transform.scale(pygame.image.load("img/koala.png").convert_alpha(), [80, 80])

if settings['ball'] == 'pool':
    paddle = Paddle(settings['paddle'], True) #jeśli tło jest białe,
else:
    paddle = Paddle(settings['paddle'], False)
bonuses = pygame.sprite.Group()

while not done:
    blocks = []
    bg_color = 0
    if settings['ball'] == 'pool':
        bg_color = 50
    round_color = np.random.randint(50-bg_color, 256-bg_color, 3).astype(int)

    for i in range(17):
        for j in range(10):
            p = np.random.uniform(0,1)
            if p < .5:
                blocks.append(Block([j * 60 + 5, 30 + i * 20], bg_color, round_color)) #normalne
            elif p > .9:
                blocks.append(Block([j * 60 + 5, 30 + i * 20], bg_color))  #bonusowe

    grid = pygame.sprite.Group(blocks)

    if path.isfile(getcwd()+"/img/"+str(round)+".png"):
        background = pygame.transform.scale(pygame.image.load("img/" + str(round) + ".jpg").convert(), SCREEN_SIZE)
    else:
        background = pygame.Surface(SCREEN_SIZE)
        if settings['ball'] == 'pool':
            background.fill([255,255,255])
        else:
            background.fill([0,0,0])

    balls = pygame.sprite.Group()
    balls.add(Ball(settings['ball'], position=paddle.get_ball_position(5), acc = float(settings['baccel'])))

    round_clock.tick()
    restart_mode = False
    paddle_accel = float(settings['paccel'])

    while not done: #runda
        screen.blit(background, (0, 0))

        keys = pygame.key.get_pressed()
        if keys[K_LEFT] and paddle.position[0] > 0:
            paddle.move(-paddle_accel)
            for ball in balls:
                if not ball.active:
                    ball.position[0] -= paddle_accel

        if keys[K_RIGHT] and paddle.position[0] < 600 - paddle.size[0]:
            paddle.move(paddle_accel)
            for ball in balls:
                if not ball.active:
                    ball.position[0] += paddle_accel

        if keys[K_k]:
            sounds["koala"].play()
            paddle_pos = paddle.position
            screen.blit(koala, (paddle_pos[0], paddle_pos[1] - 75))


        for event in pygame.event.get():
            if event.type == pygame.QUIT or keys[K_ESCAPE]:
                done = True
            if event.type == USEREVENT + 1:
                seconds += 1
            if keys[KMOD_CTRL]: # and keys[K_DOWN]:
                lives-=1
            if keys[KMOD_CTRL] and keys[K_UP]:
                lives += 1
            if keys[K_SPACE]:
                for ball in balls:
                    ball.active=True
            if keys[K_r]:       #restart
                restart_mode = True
                lives, round, score, seconds = 5, 1, 0, 0
                for i in active_bonuses:
                    active_bonuses[i] = -1
                break

        if restart_mode:
            break

        if settings['ball'] == 'pool':
            round_label = info_font.render("Round: %d" % round, 1, [180, 180, 0])
        else:
            round_label = info_font.render("Round: %d"%round, 1, [255,255,0])
        score_label = info_font.render("Score: %d" % score, 1, [200, 200, 200])
        lives_label = info_font.render("Lives:", 1, [255, 0, 0])
        screen.blit(round_label, (15, 5))
        screen.blit(score_label, (270, 5))
        screen.blit(lives_label, (470+(5-lives)*15, 5))

        for i in range(lives):
            screen.blit(heart, (580-i*15,5))

        #odbicie od paletki
        ball_on_paddle = pygame.sprite.spritecollide(paddle, balls, False)

        for ball in ball_on_paddle:
            if ball.active:
                sounds["paddle"].play()
            if sticky:
                ball.active=False
                ball.position[1] = paddle.position[1] - 2*ball.radius
            if abs(paddle.position[0]-ball.position[0]) < paddle.size[0]/5: #lewy koniec
                ball.direction[0] = -1
            elif abs(paddle.position[0]+paddle.size[0]-ball.position[0]) < paddle.size[0]/5: #prawy koniec
                ball.direction[0] = 1
            if paddle.position[1]-ball.position[1] > ball.radius/2:
                ball.direction[1] = -ball.direction[1]

        #obicia od klocków
        for ball in balls:
            collisions = pygame.sprite.spritecollide(ball, grid, False)

            for block in collisions:
                sounds["block"].play()
                grid.remove(block)
                eps = 2.7 #margines błędu aktulizacji pozycji piłki
                if block.position[1] - ball.position[1] >= ball.size[1] - eps: # odbicie u góry
                    ball.direction[1]=-1
                elif ball.position[1] + eps  >= block.position[1] + block.size[1]: #odbicie u dołu
                    ball.direction[1]=1
                elif block.position[0] + eps >= ball.position[0] + ball.size[0]: #odbicie po lewej
                    ball.direction[0]=-1
                elif ball.position[0] + eps  >= block.position[0] + block.size[0]: #odbicie po prawej
                    ball.direction[0]=1
                else:
                    ball.accel=0
                    print(ball.position, "\n", block.position)

                if block.special:
                    bonuses.add(Bonus(block.position))
                    score += 20
                else:
                    score += 10

        caught_bonuses = pygame.sprite.spritecollide(paddle, bonuses, False)

        #włączenie bonusów
        for bonus in caught_bonuses:
            sounds["bonus"].play()
            bonuses.remove(bonus)
            score+=20
            if bonus.type == 0:
                for ball in balls:
                    ball.set_parameters(2)
                    if not ball.active:
                        ball.set_position(paddle.get_ball_position(ball.radius))
                active_bonuses[0] = seconds + 10
            elif bonus.type == 1:
                for ball in balls:
                    ball.set_parameters(10)
                    if not ball.active:
                        ball.set_position(paddle.get_ball_position(ball.radius))
                active_bonuses[1] = seconds + 10
            elif bonus.type == 2:
                for ball in balls:
                    new_ball = Ball(settings['ball'], ball.position, ball.radius, ball.accel)
                    new_ball.direction = np.array([-ball.direction[0], ball.direction[1]])
                    new_ball.active = True
                    balls.add(new_ball)
                    break
            elif bonus.type == 3:
                paddle.change_size(20)
                active_bonuses[3] = seconds + 10
            elif bonus.type == 4:
                paddle.change_size(-20)
                active_bonuses[4] = seconds + 10
            elif bonus.type == 5:
                for ball in balls:
                    ball.accel += 0.3
                active_bonuses[5] = seconds + 10
            elif bonus.type == 6:
                sticky = True
                active_bonuses[6] = seconds + 10
            elif bonus.type == 7:
                if lives<10:
                    lives += 1

        #koniec czasu bonusów
        if seconds in active_bonuses.values():
            sounds["lostbonus"].play()
            for i in active_bonuses:
                if active_bonuses[i] == seconds:
                    if (i == 0 and active_bonuses[1]<=seconds) or (i==1 and active_bonuses[0]<=seconds):
                        for ball in balls:
                            ball.set_parameters(5)
                            if not ball.active:
                                ball.set_position(paddle.get_ball_position(ball.radius))
                        active_bonuses[i] -=1
                    if i == 3:
                        paddle.change_size(-20)
                        active_bonuses[i] -= 1
                    if i == 4:
                        paddle.change_size(20)
                        active_bonuses[i] -= 1
                    if i == 5:
                        for ball in balls:
                            ball.accel -= 0.3
                        active_bonuses[i] -= 1
                    if i == 6:
                        sticky = False
                        active_bonuses[i] -= 1

        #opadanie
        for bonus in bonuses:
            if not bonus.fall():
                sounds["lostbonus"].play()
                bonuses.remove(bonus)
            bonus.draw(screen)

        for ball in balls:
            if ball.move() == False:
                sounds["lostball"].play()
                balls.remove(ball)
            ball.draw(screen)

        paddle.draw(screen)
        grid.draw(screen)

        if len(balls) == 0:
            lives -= 1
            active_bonuses[5] = -1
            new_ball = Ball(settings['ball'], acc = float(settings['baccel']))
            new_ball.active = False
            new_ball.set_position(paddle.get_ball_position(ball.radius), default=True)
            balls.add(new_ball)

        if len(grid) == 0:
            round += 1
            score += 1000 - round_clock.tick()//1000
            screen.blit(end_font.render("Round "+str(round), 1, [255,255,255]), [200,320])
            pygame.display.flip()
            break

        if lives==0:
            done = True
            break

        pygame.display.flip()


sounds["gameover"].play()
newhighscore=False
if int(settings['highscore']) < score:
    newhighscore = True
    settings['highscore'] = str(score)
    with open(".settings", 'w') as f:
        for k in settings:
            f.write(k+":"+settings[k]+"\n")

while True:
    koala_over = pygame.transform.scale(pygame.image.load("img/koala.png").convert_alpha(), [450, 650])
    screen.blit(koala_over, (70, 30))
    pygame.draw.ellipse(screen, [220,220,220], Rect(108,390,420,140))
    game_over = end_font.render("GAME OVER", 1, [255, 0, 0])
    screen.blit(game_over, (150, 420))
    end_score = endscore_font.render("Your score: "+str(score), 1, [0,0,255])
    screen.blit(end_score, (205, 470))
    if newhighscore:
        pygame.draw.rect(screen, [220,220,220], Rect(385, 500, 130, 25))
        newhigh_info = info_font.render("New highscore!", 1, [255,0,0])
        screen.blit(newhigh_info, (390,505))
    pressany = mini_font.render("Press any key to exit", 1, [0,0,0])
    screen.blit(pressany, (270,510))
    totaltime = endscore_font.render(str(seconds) + "s", 1, [80, 80, 80])
    screen.blit(totaltime, (200, 100))

    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == QUIT or event.type == KEYDOWN:
            exit()
